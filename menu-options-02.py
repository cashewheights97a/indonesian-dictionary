import pickle
import random
import pprint
from tabulate import tabulate

score=0

with open ('mypickle.pickle', 'rb') as default_dict:
    b_dict=pickle.load(default_dict)
print('The retrieved dict is ')
print("================================")
pp = pprint.PrettyPrinter(indent=4)
pp.pprint(b_dict)
print("""
INDONESIAN LANGUAGE DICTIONARY FOR LEARNING AT BEGINNERS LEVEL
Have fun learning Indonesian Vocabulary ==> Make a Choice from the following:""")
print("================================")
print ('Your present Dictionary is having ', (len(b_dict.keys())), ' words')
print("================================")

ans=True
while ans:
    print("""
    1.Translate from English to Indonesian
    2.Translate from Indonesian to English
    3.Get an Indonesian word for an English word [if it is in your Dictionary]
    4.See the dictionary in table format
    5.Exit/Quit
    """)
    ans=input("What is your choice? ")


    if ans=="1":
      print("\n English ==> Indonesian Translation")
      print ("=================================")
      print('Translate the following word to Indonesian: ')
      print ("=================================")

      a=random.choice(list(b_dict))
      print(a)

      #print(b_dict[a])

      b=(b_dict[a])

      c=input('Enter the translation in Indonesian: ')

      if (c==b):
           print ('correct')
           score=score+1
      else:
           print ('incorrect')

           print('The correct translation is: ',b)
           print(" ")


    elif ans=="2":
      print("\n Indonesian ==> English Translation")
      print ("=================================")
      print('Translate the following word to English [use all small letters]  [e.g. kamu ==> you]: ')
      print ("=================================")

      a=random.choice(list(b_dict)) #key
      #print(a)

      print(b_dict[a])

      b=(b_dict[a]) #value

      
      c=input('Enter the translation in English: ') #key

      if (c==a):
           print ('correct')
           score=score+1
      else:
           print ('incorrect')

           print('The correct translation is: ',a)
           print(" ")


       
    elif ans=="3":
      print("\n English ==> Indonesian Translation")
      print ("=================================")
      d=input('Enter the English word in small letters [must be in your dictionary]: ')
      print ("=================================")

      print(b_dict[d])


    elif ans=="4":
        print ('The present Dictionary is having ', (len(b_dict.keys())), ' words')
        headers = ["ENGLISH", "INDONESIAN"]
        print(tabulate(b_dict.items(), headers = headers, tablefmt = 'grid'))


    elif ans=="5":
      print("\n Goodbye") 
      ans = None
    else:
       print("\n Not Valid Choice Try again")

print('Your final score is: ',score)
